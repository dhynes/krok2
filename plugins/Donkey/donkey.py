from errbot import BotPlugin, botcmd, re_botcmd, PY3
from errbot.utils import get_sender_username
from errbot.backends.base import get_jid_from_message
from errbot.backends.base import Identifier
from errbot.backends.base import Presence
from time import sleep
from random import randint
from collections import OrderedDict
from errbot.version import VERSION
from errbot.holder import bot
from pprint import pprint
import re
import random
import logging
import os
import sqlite3

__author__ = 'Syini666'


class Donkey(BotPlugin):

	#min_err_version = '2.2.0-beta' # Optional, but recommended
	#max_err_version = '2.2.0-beta' # Optional, but recommended
	
	# below are commands that interact with the sqlite db
	@botcmd
	def addkrok(self, msg, args):
		name = msg.mucknick
		if name == 'rockho':
			#print "rockho is attempting to add a quote, slap him!"
			self.send(msg.getFrom(),"hey fucko, you still don't run shit!",None,'groupchat')
		if name == 'hambubger':
			#print "this is also rockho attempting to add a quote, slap him!"
			self.send(msg.getFrom(),"I don't think so slick",None,'groupchat')
		else:	
			conn = sqlite3.connect('krokquotes.db')
			quote = args.replace("'","\\'")	
			sql = "INSERT INTO bestkrok VALUES (NULL, \""+str(quote)+"\");"
			#sql_clean = sql.replace("'","\\'")
			#conn.execute("INSERT INTO bestkrok VALUES (NULL,'"+quote+"')")
			conn.execute(sql)
			conn.commit()
			items = conn.execute("SELECT MAX(ID) FROM bestkrok;")
			for row in items:
				print(row[0])
				last_id = str(row[0])

			return "quote has been added successfully! id is "+last_id	
			#return sql
	
	@botcmd
	def getkrok(self, msg, args):
		args = re.sub(r'[^\x00-\x7F]+','', args)
		name = msg.mucknick
		q = ''
		if name == 'rockho':
			#print "rockho is attempting to add a quote, slap him!"
			self.send(msg.getFrom(),"hey fucko, you still don't run shit!",None,'groupchat')
		else:

			conn = sqlite3.connect('krokquotes.db')
			sp_args = args.split(' ')
			id = sp_args[0]

			sql = "SELECT id, quote FROM bestkrok WHERE id = '"+str(id)+"';"
			cursor = conn.execute(sql)
			ret = ''
			for c in cursor:
				#ret = "("+str(c[0])+") "+c[1].encode('utf-7')
				ret = "("+str(c[0])+") "+c[1]
				q = ret.replace("\\'","'")
				q.encode('ascii',errors='ignore')
			return q

	@botcmd 
	def randomkrok(self, msg, args):
		conn = sqlite3.connect('krokquotes.db')
		items = conn.execute("SELECT id, quote FROM bestkrok WHERE quote != '';")
		i = 0
		for row in items:
			#print row
			i += 1

		rnd = randint(0,i)

		quotes = conn.execute("SELECT id, quote FROM bestkrok WHERE quote != '';") 
		x = 0
		for q in quotes:
			#print(x)
			if x == rnd:
				#print("found it!")
				quote = str(q[1])
				q_id = str(q[0])
			x += 1
		return_quote = "("+q_id+") "+quote
		rq_clean = return_quote.replace("\\'","'")

		return rq_clean 


	# below are generic commands, not part of the db section

	@botcmd
	def whorunsshit(self, msg, args):
		self.send(msg.getFrom(),'not rockho!',None,'groupchat')

	@botcmd(split_args_with=' ')
	def test(self, msg, args):
		reply = "there were "+str(len(args))+" arguments"
		#self.send(msg.getFrom(),'this is a test of the emergency krokbot system!',None,'groupchat')
		self.send(msg.getFrom(),reply,None,'groupchat')

	@botcmd(split_args_with=' ')
	def deeplove(self, msg, args):

		# lets get some arguments cleaned up
		if len(args) == 1:
			# poorly formed argument set
			reply = "invalid argument(s), try again redcoat!"
			self.send(msg.getFrom(), reply, message_type=msg.getType())
		elif len(args) == 2:
			target = args[0]
			amount = int(args[1])
		
			# time to get that logfile open
			with open('rockho-improved.log') as f:
				lines = f.read().splitlines()

			# search for mentions of our target	
			i = 0
			hit = []
			for l in lines:
				match = re.search(r'(.+)'+target+'(.+)',l)
				if match:
					hit.append(l)
					i += 1
			
			# time to purge those ugly duplicates
			clean_hits = list(OrderedDict.fromkeys(hit))
			#max = len(hit) - 1	
			#print ">>>clean hits"+str(clean_hits)
			# time to do something with any hits we collected
		
			if amount > 5 and len(clean_hits) >= amount:
				amount = 5
				z = 1
				while z <= amount:
					reply = random.choice(clean_hits)
					clean_hits.remove(reply)
					self.send(msg.getFrom(), reply, message_type=msg.getType())
					z += 1	
			elif amount == 5 and len(clean_hits) >= amount:
				z = 1
				while z <= amount:
					reply = random.choice(clean_hits)
					clean_hits.remove(reply)
					self.send(msg.getFrom(), reply, message_type=msg.getType())
					z += 1	
			elif amount < 5 and len(clean_hits) >= amount:
				z = 1
				while z <= amount:
					reply = random.choice(clean_hits)
					clean_hits.remove(reply)
					self.send(msg.getFrom(), reply, message_type=msg.getType())
					z += 1	
			else:
				reply = "I'm sorry "+str(msg.mucknick)+", I'm afraid I cannot let you do that"	
				self.send(msg.getFrom(), reply, message_type=msg.getType())
	
	@botcmd
	def shootout(self, msg, args):
		test = re.search(r'([^0-9])',args)
		if test:
			reply = "try some numbers (5 or lower) instead of letters jackass!"
			self.send(msg.getFrom(), reply, message_type=msg.getType())	
		else:
			file = 'rockho-improved.log'
			num_lines = sum(1 for line in open(file))

			f = open(file)
			lines = f.readlines()
			f.close()

			max =  num_lines - 1

			#line = randint(0,max)

			limit = args[0]
			limit = int(limit)
			if limit > 5:
				limit = int(5)
				i = 1
				while i <= limit:
					line = randint(0,max)
					#return "Quoth the Krokho: ("+str(line)+") "+str(lines[line].decode('utf8'))
					reply = str(lines[line].decode('utf8'))
					self.send(msg.getFrom(), reply, message_type=msg.getType())	
					#print str(lines[line])
					i += 1
			else :
				i = 1
				while i <= limit:
					line = randint(0,max)
					#print str(lines[line])
					reply = str(lines[line].decode('utf8'))
					self.send(msg.getFrom(), reply, message_type=msg.getType())	
					i += 1


	@botcmd
	def krokquote(self, msg, args):
		file = 'rockho-improved.log'
		num_lines = sum(1 for line in open(file))

		f = open(file)
		lines = f.readlines()
		f.close()

		max =  num_lines - 1

		#line = randint(0,max)
		c = 0
		# first lets find out if the line is a command
		while c == 0:
			line = randint(0,max)
			l = lines[line]
			matchObj = re.match( r'(^\!)', l)
			#matchObj2 = re.match( r'http://www.(.+).(com|net|org|info|us)', l)
			matchObj2 = re.match( r'(.+)\.(com|net|org|info|us)(.+)', l)
			if matchObj or matchObj2:
				print("")
			else:
				# its not a command
				c = 1

		# now to determine if its a url
		url = 0

		#print "LINE:"+str(line)
		#print "LEN:"+str(len(lines[line]))
		reply = re.sub(r'[^\x00-\x7F]+','', lines[line])
		return "Quoth the Krokho: ("+str(line)+") "+str(reply.decode('utf8'))

	def my_callback(self):
		logging.debug('I am called every minute')
		odds = randint(1,3)

		if odds == 3:
			print("odds successful!")
			file = 'rockho-improved.log'
			num_lines = sum(1 for line in open(file))

			f = open(file)
			lines = f.readlines()
			f.close()

			max =  num_lines - 1

			line = randint(0,max)

			krok_msg = str(lines[line].decode('utf-8'))
			reply = re.sub(r'[^\x00-\x7F]+','', krok_msg)
			self.send('#offtopic',reply,None,'groupchat')
		else:
			print("odds failed")
			#self.send('#offtopic','failed',None,'groupchat')

	def activate(self):
		 super(donkey, self).activate()
		 self.start_poller(420, self.my_callback)
	
	# experimental functions follow
	@botcmd
	def sausage(self, msg, mess):
		
		return "is delicious!"
		
	#@re_botcmd(pattern=r"(.+)krokbot(.+)",prefixed=False, flags=re.IGNORECASE)
	@re_botcmd(pattern=r"(krokbot)",prefixed=False, flags=re.IGNORECASE)
	def listen_for_talk_of_self(self, msg, match):
	
		with open('rockho-improved.log') as f:
			lines = f.read().splitlines()
		target = msg.mucknick

		i = 0
		hit = []
		for l in lines:
			match = re.search(r'(.+)'+target+'(.+)',l)
			if match:
				hit.append(l)
				i += 1


		#response = msg.mucknick+", "+reply
		if i >= 1:	
			reply = random.choice(hit)
			reply_clean = re.sub(r'[^\x00-\x7F]+','', reply)
			self.send(msg.getFrom(), reply_clean, message_type=msg.getType())	
		else:
			self.send(msg.getFrom(), "I'm sorry, do I know you by that name?", message_type=msg.getType())


