
#!/usr/bin/python

import re
import os
import string
from random import randint

# configuration

file = 'rockho-improved.log'

num_lines = sum(1 for line in open(file))

f = open(file)
lines = f.readlines()
f.close()
max = num_lines - 1


class krokgen2():

	def count_words(line):
		words = line.split(' ')
		num_words = sum(1 for w in words)
		return num_words


	def get_words(count, target):
		w = []
		words = target.split(' ')
		#print "words: "+str(words)
		for x in range(0,count - 1):
			#print "loop iteration:"+str(x)
			#print "words left to pick from: "+str(len(words))
			rand_target = randint(0,len(words))
			rand_target -= 1
			rand_word = words[rand_target]
			#print "rand_target = "+str(rand_target)
			#print "words left to pick from: "+str(len(words))
			#print "word: "+rand_word
			w.append(rand_word)
			words.remove(rand_word)
			#print("===============")
			
		return w


	def get_rand_line():
		l = 0
		while l == 0:
			rand_int = randint(0,max)
			rand_line = lines[rand_int]
			cw = count_words(rand_line)
			# now lets make sure the line is long enough for our purposes
			if cw < 10:
				# nothing to do but try again
				l = 0
			else:
				l = cw
			clean_line = string.replace(rand_line, '\n','')
		return clean_line


	def build_limit(percentage, word_count):
		f_word_count = word_count * percentage
		fwc_rounded = round(f_word_count,0)
		r_limit = int(fwc_rounded)
		#print "r_limit is "+str(r_limit)
		return r_limit


	def krok(lmt):
		compound = []

		for x in range(1,lmt):
			rline = get_rand_line()
			rword = count_words(rline)
			#rlimit = int(round(rword * 0.20,0))
			limit = build_limit(0.20,rword)
			gw = get_words(limit, rline)
			compound.append(gw)

		st = ""

		for c in compound:
			for z in c:
				#print z
				st += " "+z

		return st

